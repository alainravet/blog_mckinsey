class Comment < ApplicationRecord
  include Commentable

  belongs_to :commentable, polymorphic: true
end

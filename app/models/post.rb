require 'csv'

class Post < ApplicationRecord
  include Commentable

  # Generate the CSV for all the Posts and their related Comments as CSV, with a header
  #
  # Result, for 2 post with 4 comments (from seeds.rb)
  #
  # header:       <--  parent,self,title,body
  # Post 1        <--  ,Post_1,"enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut ",first post
  #   Comment     <--  Post_1,Comment_1,post_1/comment - comment_1
  #   Comment     <--  Post_1,Comment_2,post_1/comment - comment_2
  # Post 2        <--  ,Post_2,"enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut ",second post
  #   Comment     <--  Post_2,Comment_4,post_2/comment - comment_4
  #   Comment     <--  Post_2,Comment_5,post_2/comment - comment_5
  #

  def self.to_csv
    CsvGenerator.new.generate(Post.all)
  end

  # Generate the CSV for the Post and its related Comments as CSV, with a header
  #
  # Result, for 1 post with 2 chained comments   (Post has_a Comment has_a Comment)
  #   parent,self,title,body
  #   ,Post_1,"enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut ",first post
  #   Post_1,Comment_1,post_1/comment - comment_1
  #   Post_1,Comment_2,post_1/comment - comment_2

  def to_csv
    CsvGenerator.new.generate(self)
  end
end

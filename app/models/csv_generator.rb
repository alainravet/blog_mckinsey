require 'csv'

class CsvGenerator

  def generate(posts)
    posts = Array(posts)
    # source: https://gorails.com/episodes/export-to-csv
    ::CSV.generate(headers: true) do |csv|
      add_header_row(csv)
      posts.each do |post|
        add_post_row(csv, post)
        post.comments.each do |comment|
          add_comment_row(csv, comment.commentable, comment)
        end
      end
    end
  end

  private

    def add_header_row(csv)
      csv << %w{ parent self title body }
    end

    def add_post_row(csv, post)
      csv << [nil, "Post_#{post.id}", post.body, post.title]
    end

    def add_comment_row(csv, commentable, comment)
      csv << ["#{commentable.class}_#{commentable.id}", "Comment_#{comment.id}", comment.body]
      comment.comments.each do |child_comment|
        add_comment_row(csv, comment, child_comment)
      end
    end
end

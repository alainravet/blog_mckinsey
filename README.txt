This is my code for the McKinsey RoR project test.

Time taken: 2 hours

I have not squashed the commits so you can follow my chain of thoughts.

Implemented:
    - seeding the development project with a few Posts and Comments
        $ rake db:seed
    - exporting CSV for 1 or all the posts, including the chained comments
    (example : /test/controllers/fixtures/expected_posts_all.csv)

Not implemented: (lack of time)
    - the comments#created action
    Note: I would have used the same technique to see the generated JSON as the one I used
        to test the generated CSV (compare to golden file)

All the features are tested:
    - models
        post_test.rb
        comment_test.rb
        fixtures_test.rb
    - rake tasks
        db:seed
    - controllers
        post_controller
        post_controller_CSV_export


------------------------------------------------------------------------------------
- work files:
    - README.txt
        this file
    - **ANALYSIS.txt
        the quick analysis I did prior to starting the project
    - **TODO.txt
        my running tasks list, updated as the project progressed.

- rails files
    models:
        Post
        Comment
        concerns/Commentable
    util
        CsvGenerator

    controllers:
        PostController
            show.csv     # exports CSV for all the posts
            index.csv    # exports CSV for 1 post
            show.text    # shows in the browser the CSV for all the posts
            index.text   # shows in the browser the CSV for 1 post

require 'test_helper'
require 'rake'
class DbSeedTest < ActiveSupport::TestCase
  test "rake db:seed creates 2 post and 5 comments" do
    [Post, Comment].each(&:destroy_all)

    BlogMckinsey::Application.load_tasks
    Rake::Task['db:seed'].invoke

    assert_equal 2, Post.count
    assert_equal 5, Comment.count
  end

  test "rake db:seed is not idempottent" do
    skip
    [Post, Comment].each(&:destroy_all)

    BlogMckinsey::Application.load_tasks

    Rake::Task['db:seed'].invoke

    BlogMckinsey::Application.load_tasks
    Rake::Task['db:seed'].invoke
# DOES NOT WORK- WHY?
    assert_equal 2+2, Post.count
    assert_equal 5+5, Comment.count
  end
end

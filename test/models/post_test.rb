require 'test_helper'

class PostTest < ActiveSupport::TestCase
  test "a fresh post has no comments" do
    p = Post.create(title: "zzz", body: "iii")
    assert p.comments.empty?
  end

  test "a post with 2 direct comments" do
    p = Post.create(title: "zzz", body: "iii")
    p.comments << Comment.new(body: 'comm1')
    p.comments << Comment.new(body: 'comm2')
    assert_equal Comment.last(2),
                 p.comments
  end

  test "a post with 1 comment that has a comment" do
    p = Post.create(title: "zzz", body: "iii")
    p.comments << Comment.new(body: 'comm1')
    new_comment = Comment.last
    new_comment.comments << Comment.new(body: 'comm2')
    assert_equal [Comment.last(2).first],
                 p.comments
    assert_equal [Comment.last(2).last],
                 new_comment.comments
  end
end

require 'test_helper'

class FixtureTest < ActiveSupport::TestCase
  test "Post one has 2 chained comments" do
    assert_equal [comments(:one)], posts(:one).comments
    assert_equal [comments(:two)], posts(:one).comments.first.comments
  end
end

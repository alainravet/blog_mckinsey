require 'test_helper'

class CommentTest < ActiveSupport::TestCase
  test "a fresh Comment has no comments" do
    p = Post.create(title: "zzz", body: "iii")
    p.comments << Comment.new(body: 'a-comment')

    new_comment = Comment.last
    assert_equal [new_comment], p.comments
    assert_equal [],            new_comment.comments
  end

  test "a Post's Comment can have a Comment of its own" do
    p = Post.create(title: "zzz", body: "iii")
    p.comments << Comment.new(body: 'comm1')
    parent_comment = Comment.last
    parent_comment.comments << Comment.new(body: 'comm2')
    child_comment = Comment.last

    assert_equal [parent_comment], p.comments
    assert_equal [child_comment ], parent_comment.comments
  end
end

require 'test_helper'

class PostsControllerCsvExportTest < ActionDispatch::IntegrationTest
  test "exporting the CSV for 1 Post" do
    get post_url(posts(:one), format: :csv)

    exptected_csv = File.read("#{Rails.root}/test/controllers/fixtures/expected_post_one.csv")
    assert_equal exptected_csv, response.body
  end

  test "exporting the CSV for all Posts" do
    get posts_url(format: :csv)

    exptected_csv = File.read("#{Rails.root}/test/controllers/fixtures/expected_posts_all.csv")
    assert_equal exptected_csv, response.body
  end
end

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
post_1 = Post.create!(title: 'first post', body: 'enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut ')
comment_1 = Comment.create!(commentable: post_1,    body: "post_1/comment - comment_1")
comment_2 = Comment.create!(commentable: post_1,    body: "post_1/comment - comment_2")
comment_3 = Comment.create!(commentable: comment_1, body: "post_1/comment_1/comment - comment_3")

post_2 = Post.create!(title: 'second post', body: 'enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut ')
Comment.create!(commentable: post_2,    body: "post_2/comment - comment_4")
Comment.create!(commentable: post_2,    body: "post_2/comment - comment_5")

puts "#{Post.count} posts and #{Comment.count} comments."
